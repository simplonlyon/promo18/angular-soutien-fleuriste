# Projet Fleuriste

## Ce que je dois faire

  * Afficher la liste des fleurs en vente
  * Afficher la page d'une fleur avec tous les détails
  * Pouvoir se connecter pour ajouter/modifier des fleurs

## Traduction en Angular

  * Je veux manipuler des fleurs : j'ai une **entité Fleur**
  * Je veux pouvoir me connecter : j'ai une **entité User**
  * Pour stocker les fleurs, j'ai besoin d'un **service FleurService**
  * Pour stocker les users, j'ai besoin d'un **service UserService**
  * Pour gérer l'authentification, j'ai besoin d'un **service AuthService**
  * Pour afficher la liste des fleurs du site, j'ai besoin d'un **composant ListeFleursComponent**
  * Pour afficher une fleur avec tous ses détails, j'ai besoin d'un **composant FleurComponent**

## Règle habituelle : 
  * quand je veux **afficher** quelque chose, j'ai besoin d'un **composant**
  * ce quelque chose, c'est souvent une **entité**
  * pour gérer le stockage de ce quelque chose, j'ai besoin d'un **service**

## Lien avec Java
  * un service dans Angular ~= un repo dans Spring Boot
  * un composant dans Angular ~= un controlleur dans Java (ça utilise l'entité, et ça utilise le service/le repo pour accéder au stockage)
  * Quand j'ai une entité en Java, j'ai la même en Angular