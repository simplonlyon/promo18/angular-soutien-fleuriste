import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListeFleursComponent } from './liste-fleurs/liste-fleurs.component';
import { FicheFleurComponent } from './fiche-fleur/fiche-fleur.component';

@NgModule({
  declarations: [
    AppComponent,
    ListeFleursComponent,
    FicheFleurComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
