import { TestBed } from '@angular/core/testing';

import { FleurService } from './fleur.service';

describe('FleurService', () => {
  let service: FleurService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FleurService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
