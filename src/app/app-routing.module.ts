import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FicheFleurComponent } from './fiche-fleur/fiche-fleur.component';
import { ListeFleursComponent } from './liste-fleurs/liste-fleurs.component';

//Le path commence après http://localhost:4200/
// => il ne faut pas mettre le / au début des chemins ici !
const routes: Routes = [
  { path: "", component: ListeFleursComponent },
  { path: "fleur/:id", component: FicheFleurComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
