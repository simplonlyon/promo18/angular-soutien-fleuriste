import { Component, OnInit } from '@angular/core';
import { Fleur } from '../entities';
import { FleurService } from '../fleur.service';

@Component({
  selector: 'app-fiche-fleur',
  templateUrl: './fiche-fleur.component.html',
  styleUrls: ['./fiche-fleur.component.css']
})
export class FicheFleurComponent implements OnInit {

  fleur:Fleur = {
    id: 1,
    name: "Jacqueline",
    variety: "Rose",
    color: "pink",
    price: 2.50
  };

  constructor() { }

  ngOnInit(): void {
  }

}
