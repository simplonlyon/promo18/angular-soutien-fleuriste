import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheFleurComponent } from './fiche-fleur.component';

describe('FicheFleurComponent', () => {
  let component: FicheFleurComponent;
  let fixture: ComponentFixture<FicheFleurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FicheFleurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheFleurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
