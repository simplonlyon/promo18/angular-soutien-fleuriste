import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeFleursComponent } from './liste-fleurs.component';

describe('ListeFleursComponent', () => {
  let component: ListeFleursComponent;
  let fixture: ComponentFixture<ListeFleursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeFleursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeFleursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
