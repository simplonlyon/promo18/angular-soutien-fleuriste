import { Component, OnInit } from '@angular/core';
import { Fleur } from '../entities';

@Component({
  selector: 'app-liste-fleurs',
  templateUrl: './liste-fleurs.component.html',
  styleUrls: ['./liste-fleurs.component.css']
})
export class ListeFleursComponent implements OnInit {

  //On crée notre liste de fleur en utilisant la syntaxe JSON
  fleurs:Fleur[] = [
    {
      id: 145,
      name:"Belle des prés",
      variety: "Marguerite",
      price: 12.5,
      color: "Blanc"
    },
    {
      id: 2,
      name:"Jeanette",
      variety: "Anémone de mer",
      price: 249.99,
      color: "Multicolore"
    },
    {
      id: 3,
      name:"Henriette",
      variety: "Rose",
      price: 4.5,
      color: "Jaune"
    },
  ];
  /*
    fleurs[0] = > "Belle des prés"
    fleurs[1] = > "Jeanette"
    fleurs[2] = > "Henriette"

  */

  constructor() { }

  ngOnInit(): void {
  }

}
