export interface Fleur {
    id: number,
    name:string,
    variety:string,
    price:number,
    color:string
}