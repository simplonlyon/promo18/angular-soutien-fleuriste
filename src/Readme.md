# Soutien Angular

## Étapes à suivre : 

1. Créer le projet vide sur GitLab
2. Créer le projet Angular en local : ```ng g new angular-soutien-fleuriste```
3. Relier le projet Angular au dépôt GitLab
    3.1 Aller sur la page du projet sur gitlab
    3.2 Cliquer sur le bouton bleu "Clone"
    3.3 Copier l'URL "clone with SSH"
    3.4 Ouvrir le projet dans VS Code
    3.5 Aller sur le menu Git (Contrôle de code source) à gauche
    3.6 Cliquer sur le menu "..." au-dessus de la case pour mettre le message de commit
    3.7 Aller dans le menu "À distance..."
    3.8 Cliquer sur "Ajouter un dépôt distant..."
    3.9 Dans la palette de commande, quand il demande "Indiquez l'URL du dépôt, ou choisisser une source de dépôt", coller l'URL de "Clone with SSH" récupérée à l'étape 3.3
    3.10 Dans la palette de commande, quand il demande le "Nom du dépôt distant", indiquer "origin"
    3.11 Faire un premier commit sur le projet
    3.12 Pusher le premier commit
4. Nettoyer le app.component.html par défaut
5. Créer les composants avec ```ng g c $COMPOSANT```
6. Créer les services avec ```ng g s $SERVICE```
7. Créer le fichier entities.ts dans le dossier src/app/
8. Faire un commit
9. Pusher
10. Créer les routes dans le fichier src/app/app-routing.module.ts
    * les routes sont **un lien** entre **une adresse URL** et **un composant Angular**
    * le format est ```{ path: "$ROUTE", component: $COMPOSANT }```
    * pour la route par défaut, laisser $ROUTE vide : ```{ path: "", component: COMPOSANT DE LA PAGE D'ACCUEIL }
    * Attention : la variable "const routes" est un tableau, il faut donc séparer chaque route par une virgule